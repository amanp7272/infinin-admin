const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tagSchema = new Schema({
	tag: {
		type: Schema.Types.ObjectId,
		ref: 'Filter'
	}
});

module.exports = mongoose.model('Tag', tagSchema);
