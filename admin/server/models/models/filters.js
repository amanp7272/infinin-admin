const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const filterSchema = new Schema(
	{
		name: {
			type: String,
			required: true
		},
		status: {
			type: Boolean,
			required: true
		},
		topicType: {
			type: String,
			required: true
		}
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

module.exports = mongoose.model('Filter', filterSchema);
