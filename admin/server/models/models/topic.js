const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const topicSchema = new Schema({
	topic: {
		type: Schema.Types.ObjectId,
		ref: 'Filter'
	}
});

module.exports = mongoose.model('Topic', topicSchema);
