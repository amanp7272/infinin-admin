const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categoryTreeSchema = new Schema({
	_Id: {
		type: Schema.Types.ObjectId,
		ref: 'Filter'
	},
	path: {
		type: String
	}
});

module.exports = mongoose.model('CategoryTree', categoryTreeSchema);
