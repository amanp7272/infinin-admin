const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const app = express();
const readExcel = require('../server/service/excelService');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use("/", indexRouter);
// app.use("/users", usersRouter);

// CORS ERROR HANDLING
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-with, Content-Type, Accept, Authorization');
	if (req.method === 'OPTIONS') {
		res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
		return res.status(200).json({});
	}
	next();
});

mongoose
	.connect(
		`mongodb+srv://infininDemo:1LXqwVp8@infinindemo.elbzn.mongodb.net/infinin-dev?retryWrites=true&w=majority`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
	.then(() => {
		console.log('Mongoose is connected succesfully!');
	})
	.catch((err) => {
		console.log(`There is an error while connecting mongo server - Error: ${err}`);
	});
app.get('/admin', (req, res) => {
	res.send('This is Admin page');
});
app.post('/admin', (req, res) => {
	readExcel(req.body);
	//console.log(req.body);
});

module.exports = app;
