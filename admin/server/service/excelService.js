const CategoryTree = require('../models/models/categoryTree');
const Filter = require('../models/models/filters');
const Category = require('../models/models/category');
const Topic = require('../models/models/topic');
const Tag = require('../models/models/tag');

const readExcel = async (datas) => {
	try {
		var filter;
		for (var data of datas) {
			if (data) {
				var topicType = data.topicType;
				var topicName = data.topicName;
				var topicPath = data.topicPath;
				const fill = new Filter({
					name: topicName,
					status: true,
					topicType: topicType
				});
				const _result = await fill.save();
				const createdFilter = {
					..._result._doc,
					_id: _result.id
				};
				if (topicType === 'category') {
					const category = new Category();
					category.category = createdFilter._id;
					filter = createdFilter._id;
					await category.save();
				} else if (topicType === 'topic') {
					const topic = new Topic();
					topic.topic = createdFilter._id;
					filter = createdFilter._id;
					await topic.save();
				} else if (topicType === 'tag') {
					const tag = new Tag();
					tag.tag = createdFilter._id;
					filter = createdFilter._id;
					await tag.save();
				} else {
					print(' Invalid Data');
					return null;
				}
			} else {
				console.log('Empty Row');
			}

			const categoryTree = new CategoryTree({
				_id: filter,
				path: topicPath !== '' ? topicPath : null
			});
			await categoryTree.save();
			//const result =
			// const createdcategoryTree = {
			// 	...result._doc
			// 	//_id: _filter.bind(this, result._id)
			// };
			console.log('CategoryTree save');

			//return createdcategoryTree;
		}
	} catch (err) {
		throw new Error(err);
	}
};

module.exports = readExcel;
