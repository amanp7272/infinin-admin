// // const formElem = document.getElementById("formElem");
// // const myFile = document.getElementById("myFile");
// // const uploadBtn = document.getElementById("upload-btn");

// // formElem.addEventListener("submit", (e) => {
// //   e.preventDefault();

// //   const url = "http://localhost:3031/admin";
// //   let formData = new FormData();

// //   console.log(myFile.files[0]);

// //   formData.append("myFile", myFile.files[0]);

// //   console.log(formData);
// //   fetch(url, {
// //     method: "POST",
// //     body: formData,
// //   }).then((response) => {
// //     console.log(response);
// //   });
// // });

let selectedFile;
console.log(window.XLSX);
document.getElementById("input").addEventListener("change", (event) => {
  selectedFile = event.target.files[0];
  console.log(selectedFile);
});

document.getElementById("button").addEventListener("click", (e) => {
  console.log(selectedFile);
  const reader = new FileReader();
  reader.readAsBinaryString(selectedFile);
  reader.onload = (e) => {
    const data = e.target.result;
    const workbook = XLSX.read(data, {
      type: "binary",
    });
    console.log(workbook);
    workbook.SheetNames.forEach((sheetName) => {
      const XL_row_object = XLSX.utils.sheet_to_row_object_array(
        workbook.Sheets[sheetName]
      );
      const json_object = JSON.stringify(XL_row_object);
      console.log(json_object);
      document.getElementById("jsondata").innerHTML = json_object;
    });
  };
  reader.onerror = (e) => {
    console.error(`File could not be read! Code: ${event.target.error.code}`);
  };
});
