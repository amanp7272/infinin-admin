let selectedFile;
let json_object;
document.getElementById("input").addEventListener("change", (event) => {
  selectedFile = event.target.files[0];

  let fileReader = new FileReader();

  fileReader.readAsBinaryString(selectedFile);

  fileReader.onload = (event) => {
    let data = event.target.result;
    let workbook = XLSX.read(data, { type: "binary" });

    console.log(workbook);

    workbook.SheetNames.forEach((sheet) => {
      let rowObject = XLSX.utils.sheet_to_row_object_array(
        workbook.Sheets[sheet]
      );

      console.log(rowObject);

      json_object = JSON.stringify(rowObject);
      // console.log(json_object);
      document.getElementById("jsondata").innerHTML = JSON.stringify(
        rowObject,
        undefined,
        4
      );
    });
  };
});

document.getElementById("button").addEventListener("click", () => {
  let url = "http://localhost:3031/admin";
  let body = json_object;

  console.log(body);
  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: body,
  }).then((response) => {
    console.log(response);
  });
});
